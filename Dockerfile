FROM node:14
MAINTAINER Ben Donnelly <ben_donnelly@intergral.com>

ENV NV_NAME=typescript-test-app
ENV NV_REPO_URL=https://gitlab.com/nerd-vision/test-apps/node/typescript

# Add our service
ADD . /app
WORKDIR /app

ENTRYPOINT ["npm", "run", "start"]
